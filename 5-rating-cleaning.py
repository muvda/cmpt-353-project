import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sb


def value_to_binary(value):
    if value is not None:
        return 1
    else:
        return 0


def plot_missing_value(data):
    plt.close("all")
    plt.figure(figsize=(10, 6))
    plt.xticks(rotation=25)
    missing_values = data.isnull().sum()
    plt.bar(missing_values.index, missing_values)
    plt.title('No. missing values for each features')
    plt.ylabel('No. Missing Values')
    plt.savefig('figures/Cleaning/no_missing_values.png')
    plt.close("all")


def drop_unused_columns(data):
    data = data.drop(['timestamp', 'address', 'wikidata', 'amenity'], axis=1)
    return data


def plot_rating(data):
    plt.close('all')
    plt.hist(data['rating'])
    plt.title('Distribution of rating')
    plt.xlabel('Rating')
    plt.ylabel('Frequency')
    plt.savefig('./figures/Cleaning/distribution_of_rating.png')
    plt.close("all")


def plot_rating_labels(data):
    rating = data['rating_label'].value_counts()
    plt.close('all')
    plt.bar(rating.index, rating)
    plt.title("Distribution of rating labels")
    plt.ylabel('Frequency')
    plt.savefig('./figures/Cleaning/distribution_rating_labels.png')


def mapping(data):
    data['phone'] = data['phone'].apply(value_to_binary)
    data['website'] = data['website'].apply(value_to_binary)

    price_map = {
        '$': 1,
        '£': 1,
        '$$': 2,
        '€€': 2,
        '££': 2,
        '$$$': 3,
        '$99': 3,
        '$87': 3,
        '$$$$': 4,
        '$198': 4,
        'IDR 4,258,078': 4
    }
    data['price'] = data['price'].map(price_map)

    binary_mapping = {
        True: 1,
        False: 0
    }
    data['delivery'] = data['delivery'].map(binary_mapping)
    data['dine_in'] = data['dine_in'].map(binary_mapping)
    data['drive_through'] = data['drive_through'].map(binary_mapping)
    return data


def impute(data):
    data['chain'] = data['chain'].fillna(0)
    data['price_semi'] = data['price'].fillna(-1)
    data['price'] = data['price'].fillna(data['price'].median())
    data = data.dropna()
    return data


def scale_ratings(data):
    data['rating_label'] = data['rating'].apply(classify_rating)
    data = data.dropna()
    plot_rating_labels(data)
    return data


def classify_rating(rating):
    if rating < 3.0:
        return 'mediocre'
    elif 3.0 <= rating < 4.0:
        return 'average'
    elif 4.0 <= rating:
        return 'recommended'


def main(inputs):
    sb.set()

    data = pd.read_json(inputs, orient='records', lines=True)

    print('Remove unused columns')
    data = drop_unused_columns(data)

    print('Plot missing data')
    plot_missing_value(data)

    print('Mapping data to value')
    data = mapping(data)

    print('Impute missing data')
    data = impute(data)

    print('Plot rating')
    plot_rating(data)

    print('Classifying the rating')
    data = scale_ratings(data)

    data.to_json('data/osm_food_rating_cleaned.json.gz', orient='records', lines=True)


if __name__ == '__main__':
    main('data/osm_food_rating_extract.json.gz')
