import pandas as pd
import numpy as np
from GPSPhoto import gpsphoto
import sys


def distance(lon1, lat1, lon2, lat2):
    R = 6371000
    dLat = np.deg2rad(lat2 - lat1)
    dLon = np.deg2rad(lon2 - lon1)
    a = np.sin(dLat / 2) * np.sin(dLat / 2) + np.cos(np.deg2rad(lat1)) * np.cos(np.deg2rad(lat2)) * np.sin(
        dLon / 2) * np.sin(dLon / 2)
    c = 2 * np.arctan2(np.sqrt(a), np.sqrt(1 - a))
    d = R * c
    return d


def extract_image(image):
    exif_data = gpsphoto.getGPSData(image)
    lat = exif_data['Latitude']
    lon = exif_data['Longitude']
    if lat is None or lon is None:
        raise Exception("Can't obtain the photo GPS data!")
    return lon, lat


def get_closet_places(data, dist_threshold):
    filtered_data = data[data['distance'] <= dist_threshold].copy()
    return filtered_data[['name', 'amenity', 'distance', 'lat', 'lon']]


def main(inputs, image='photo/photo4.jpg', dist_threshold=400):
    data = pd.read_json(inputs, orient='records', lines=True)
    lon2, lat2 = extract_image(image)
    data['distance'] = distance(data['lon'], data['lat'], lon2, lat2)
    filtered_data = get_closet_places(data, dist_threshold)
    print(filtered_data.set_index('distance').sort_index())


if __name__ == '__main__':
    main('data/osm_tourist.json.gz', sys.argv[1], int(sys.argv[2]))
