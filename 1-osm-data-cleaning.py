import pandas as pd
import numpy as np
import geopandas as gpd
from shapely.geometry import Point

tourist_amenity = ['cafe', 'fast_food', 'place_of_worship', 'restaurant', 'pub', 'community_centre', 'cinema',
                   'theatre', 'ferry_terminal', 'library', 'bar', 'clinic', 'childcare', 'public_bookcase',
                   'university', 'arts_centre', 'ice_cream', 'bureau_de_change', 'social_facility', 'clock',
                   'marketplace', 'nightclub', 'hospital', 'gambling', 'townhall', 'music_school', 'bistro',
                   'meditation_centre', 'events_venue', 'food_court', 'fountain', 'juice_bar', 'internet_cafe',
                   'social_centre', 'studio', 'lounge', 'spa', 'chiropractor', 'monastery', 'gym', 'casino',
                   'shop|clothes', 'research_institute', 'leisure']

food_amenity = ['cafe', 'fast_food', 'restaurant', 'pub', 'bar', 'ice_cream']


def remove_empty_tags(data):
    def check_empty_tags(tag):
        if len(tag) == 0:
            return None
        return tag

    data['tags'] = data['tags'].apply(check_empty_tags)
    data = data.dropna()
    return data


def filtered_amenity(data, lists):
    def keep_amenity(amenity, lists):
        if amenity in lists:
            return amenity
        return None

    data['amenity'] = data['amenity'].apply(keep_amenity, lists=lists)
    data = data.dropna()
    return data


def find_address_or_remove(data, geo_data):
    def get_street(tag):
        if 'addr:street' in tag:
            return tag['addr:street']
        return None

    def get_city(record, geo_data):
        tag = record['tags']
        point = Point(record['lon'], record['lat'])
        polygon = geo_data['geometry']
        if 'addr:city' in tag:
            return tag['addr:city']
        else:
            result = polygon.contains(point)
            filtered_gd = geo_data[result.values]
            if len(filtered_gd) == 1:
                city = filtered_gd.iloc[0]['CENSUS_SUBDIVISION_NAME']
                return city
            else:
                return None

    working_data = data.copy()
    working_data['street'] = data['tags'].apply(get_street).copy()
    working_data = working_data.dropna()
    working_data['city'] = data.apply(get_city, geo_data=geo_data, axis=1).copy()
    working_data = working_data.dropna()
    return working_data


def data_cleaning(data):
    cleaned_data = data.copy().dropna()
    cleaned_data = remove_empty_tags(cleaned_data)

    tourist_data = filtered_amenity(cleaned_data, tourist_amenity)
    food_data = filtered_amenity(cleaned_data, food_amenity)
    # food_data = find_address_or_remove(food_data, geo_data)

    return tourist_data, food_data


def main(inputs):
    data = pd.read_json(inputs, orient='records', lines=True)
    tourist_data, food_data = data_cleaning(data)
    tourist_data.to_json('data/osm_tourist.json.gz', orient='records', lines=True)
    food_data.to_json('data/osm_food.json.gz', orient='records', lines=True)


if __name__ == '__main__':
    main('data/amenities-vancouver.json.gz')
