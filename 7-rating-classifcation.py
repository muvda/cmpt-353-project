import numpy as np
import pandas as pd
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import AdaBoostClassifier, GradientBoostingClassifier, RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.feature_selection import RFECV
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import train_test_split, KFold
import matplotlib.pyplot as plt
from sklearn.metrics import accuracy_score, ConfusionMatrixDisplay, confusion_matrix, balanced_accuracy_score

OUTPUT_TEMPLATE = (
    'Accuracy score: {ac_valid:.3f} (valid), {ac_train:.3f} (train)\n'
    'Balance accuracy score: {bac_valid:.3f} (valid), {bac_train:.3f} (train)'
)


def validation(model, name, X_train, X_valid, y_train, y_valid):
    train_predict = model.predict(X_train)
    valid_predict = model.predict(X_valid)
    print(OUTPUT_TEMPLATE.format(
        ac_valid=accuracy_score(y_valid, valid_predict),
        ac_train=accuracy_score(y_train, train_predict),
        bac_valid=balanced_accuracy_score(y_valid, valid_predict),
        bac_train=balanced_accuracy_score(y_train, train_predict)
    ))
    labels = ['mediocre', 'average', 'recommended']

    confmatrix_train = confusion_matrix(y_train, train_predict, labels=labels)
    confmatrix_valid = confusion_matrix(y_valid, valid_predict, labels=labels)

    plt.close('all')
    display = ConfusionMatrixDisplay(confusion_matrix=confmatrix_valid)
    display.plot()
    plt.title(name + 'Classifer Confusion Matrix on the Valid Data')
    plt.savefig('./figures/Classification/' + name + 'classifier_confusion_matrix_valid_data.png')
    plt.close('all')

    display = ConfusionMatrixDisplay(confusion_matrix=confmatrix_train)
    display.plot()
    plt.title(name + 'Classifer Confusion Matrix on the Train Data')
    plt.savefig('./figures/Classification/' + name + 'classifier_confusion_matrix_train_data.png')
    plt.close('all')


def run_different_model(X, y):
    print('Begin model building')
    X_train, X_valid, y_train, y_valid = train_test_split(X, y, train_size=0.80, test_size=0.20,
                                                          random_state=1)  # 80,20

    print('Building GaussianNB Classifier model')
    model_gnb = make_pipeline(
        StandardScaler(),
        GaussianNB()
    )
    model_gnb.fit(X_train, y_train)
    validation(model_gnb, 'GaussianNB', X_train, X_valid, y_train, y_valid)

    print('Building KNeighbors Classifier model')
    model_knn = make_pipeline(
        StandardScaler(),
        KNeighborsClassifier(n_neighbors=10)
    )
    model_knn.fit(X_train, y_train)
    validation(model_knn, 'KNeighbors', X_train, X_valid, y_train, y_valid)

    print('Building AdaBoost Classifier model')
    model_ab = make_pipeline(
        StandardScaler(),
        AdaBoostClassifier(n_estimators=500, learning_rate=1, random_state=np.random.seed(1234))
    )
    model_ab.fit(X_train, y_train)
    validation(model_ab, 'AdaBoost', X_train, X_valid, y_train, y_valid)

    print('Building GradientBoosting Classifier model')
    model_gb = make_pipeline(
        StandardScaler(),
        GradientBoostingClassifier(n_estimators=500, max_depth=7, min_samples_leaf=0.1,
                                   random_state=np.random.seed(1234))
    )
    model_gb.fit(X_train, y_train)
    validation(model_gb, 'GradientBoosting', X_train, X_valid, y_train, y_valid)

    print('Building RandomForest Classifier model')
    model_rf = make_pipeline(
        StandardScaler(),
        RandomForestClassifier(n_estimators=500, criterion='gini', max_depth=7, min_samples_leaf=30,
                               bootstrap=True, random_state=np.random.seed(1234), class_weight='balanced')
    )
    model_rf.fit(X_train, y_train)
    validation(model_rf, 'RandomForest', X_train, X_valid, y_train, y_valid)

    print('Building MLP Classifier model')
    model_mlp = make_pipeline(
        StandardScaler(),
        MLPClassifier(solver='lbfgs', hidden_layer_sizes=(4,),
                      activation='logistic', max_iter=1000, random_state=np.random.seed(1234))
    )
    model_mlp.fit(X_train, y_train)
    validation(model_mlp, 'MLP', X_train, X_valid, y_train, y_valid)


def plot_feature_important(estimator, features, X_train, y_train):
    # From https://scikit-learn.org/stable/auto_examples/ensemble/plot_forest_importances.html
    estimator.fit(X_train, y_train)
    importances = estimator.feature_importances_
    std = np.std([
        tree.feature_importances_ for tree in estimator.estimators_], axis=0)
    plt.close("all")
    plt.figure(figsize=(8, 6))
    plt.xticks(rotation=25)
    plt.bar(features, importances, yerr=std)
    plt.ylabel("Mean decrease in impurity")
    plt.title('Feature importances using MDI')
    plt.savefig('figures/Classification/classifier_feature_importances.png')
    plt.close("all")


def features_selection_rfecv(X, y):
    print('Begin feature selection')
    features = X.columns.tolist()
    X_norm = StandardScaler().fit_transform(X)
    X_train, X_valid, y_train, y_valid = train_test_split(X_norm, y, random_state=1, test_size=0.3)
    rfecv_estimator = RandomForestClassifier(random_state=1)
    plot_feature_important(rfecv_estimator, features, X_train, y_train)
    rfecv_selector = RFECV(estimator=rfecv_estimator, cv=KFold(2), scoring='accuracy', min_features_to_select=1)
    rfecv_selector = rfecv_selector.fit(X_train, y_train)
    mask = rfecv_selector.get_support()
    selected_features = X.loc[:, mask].columns.tolist()
    print('Selected features are ' + ', '.join(selected_features))
    return selected_features


def main(inputs):
    data = pd.read_json(inputs, orient='records', lines=True)
    X = data[
        ['lat', 'lon', 'num_reviews', 'phone', 'website', 'price', 'chain', 'delivery', 'dine_in', 'drive_through']]
    y = data['rating_label']
    chosen_features = features_selection_rfecv(X, y)
    X_select = X[chosen_features].to_numpy()
    run_different_model(X_select, y.to_numpy())


if __name__ == '__main__':
    main('data/osm_food_rating_cleaned.json.gz')
