import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, balanced_accuracy_score
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.feature_selection import RFECV
from sklearn.model_selection import train_test_split, KFold
from sklearn.semi_supervised import SelfTrainingClassifier
import matplotlib.pyplot as plt
import seaborn as sb

OUTPUT_TEMPLATE = (
    'Accuracy score: {ac_valid:.3f} (valid), {ac_train:.3f} (train)\n'
    'Balance accuracy score: {bac_valid:.3f} (valid), {bac_train:.3f} (train)'
)


def semi_classify_price(data):
    print('Begin Semi-supervised imputation')
    X = data[['lat', 'lon', 'num_reviews', 'phone', 'website', 'chain', 'delivery', 'dine_in', 'drive_through']]
    y = data['price_semi']
    model_rf = make_pipeline(
        StandardScaler(),
        RandomForestClassifier(n_estimators=110, criterion='gini', max_depth=7, min_samples_leaf=30,
                               bootstrap=True, random_state=np.random.seed(1234), class_weight='balanced')
    )
    stc = SelfTrainingClassifier(base_estimator=model_rf, criterion='k_best', k_best=100, max_iter=50)
    stc.fit(X, y)
    y_predict = stc.predict(X)

    price = []
    for row, predict in zip(data['price_semi'], y_predict):
        if row == -1:
            price.append(predict)
        else:
            price.append(row)
    data['price'] = price
    return data


def features_selection_rfecv(X, y):
    print('Begin feature selection')
    X_norm = StandardScaler().fit_transform(X)
    X_train, X_valid, y_train, y_valid = train_test_split(X_norm, y, random_state=1, test_size=0.3)
    rfecv_estimator = RandomForestClassifier(random_state=1)
    rfecv_selector = RFECV(estimator=rfecv_estimator, cv=KFold(2), scoring='accuracy', min_features_to_select=1)
    rfecv_selector = rfecv_selector.fit(X_train, y_train)
    mask = rfecv_selector.get_support()
    selected_features = X.loc[:, mask].columns.tolist()
    print('Selected features are ' + ', '.join(selected_features))
    return selected_features


def classification_building(data):
    X = data[
        ['lat', 'lon', 'num_reviews', 'phone', 'website', 'price', 'chain', 'delivery', 'dine_in', 'drive_through']]
    y = data['rating_label']
    chosen_features = features_selection_rfecv(X, y)
    print('Begin building model')
    X_select = X[chosen_features].to_numpy()
    X_train, X_valid, y_train, y_valid = train_test_split(X_select, y.to_numpy(), train_size=0.80, test_size=0.20,
                                                          random_state=1)
    model_rf = make_pipeline(
        StandardScaler(),
        RandomForestClassifier(n_estimators=500, criterion='gini', max_depth=7, min_samples_leaf=30,
                               bootstrap=True, random_state=np.random.seed(1234), class_weight='balanced')
    )
    model_rf.fit(X_train, y_train)
    train_predict = model_rf.predict(X_train)
    valid_predict = model_rf.predict(X_valid)
    print(OUTPUT_TEMPLATE.format(
        ac_valid=accuracy_score(y_valid, valid_predict),
        ac_train=accuracy_score(y_train, train_predict),
        bac_valid=balanced_accuracy_score(y_valid, valid_predict),
        bac_train=balanced_accuracy_score(y_train, train_predict)
    ))


def plot_imputation(simple_imputed, semi_classify_imputed):
    plt.close('all')
    plt.hist([simple_imputed, semi_classify_imputed])
    plt.title('Distribution of price between imputation methods')
    plt.legend(['Median imputation', 'Semi-supervised imputation'])
    plt.xlabel('Price')
    plt.ylabel('Frequency')
    plt.savefig('./figures/Semi_classify/distribution_imputed_price.png')


def imputation_compare(data_simple_imputed, data_semi_classify):
    plot_imputation(data_simple_imputed['price'], data_semi_classify['price'])
    print('Building RandomForest on Median imputation')
    classification_building(data_simple_imputed)
    print('Building RandomForest on Semi-supervised imputation')
    classification_building(data_semi_classify)


def main(inputs):
    sb.set()
    data_semi_classify = pd.read_json(inputs, orient='records', lines=True)
    data_simple_imputed = pd.read_json(inputs, orient='records', lines=True)
    data_semi_classify = semi_classify_price(data_semi_classify)
    imputation_compare(data_simple_imputed, data_semi_classify)


if __name__ == '__main__':
    main('data/osm_food_rating_cleaned.json.gz')
