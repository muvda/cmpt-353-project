import numpy as np
import pandas as pd
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import AdaBoostClassifier,GradientBoostingClassifier,RandomForestClassifier
from sklearn.preprocessing import MinMaxScaler, StandardScaler
from sklearn.feature_selection import RFECV
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import train_test_split,KFold
from sklearn.svm import SVR


def classify_rating(rating):
    if 0.00 <= rating < 0.60:
        return 'negative'
    elif 0.60 <= rating <= 0.80:
        return 'neutral'
    elif 0.80 < rating <= 1.00:
        return 'positive'
    else:
        return None
    

def run_models(X,y):
    #GAUSSIAN NB
   
    X_train, X_valid, y_train, y_valid = train_test_split(X, y,train_size = 0.80, test_size = 0.20, random_state=1)  #80,20
      
    model_gnb = make_pipeline( 
        StandardScaler(),
        GaussianNB()
        )
     
    model_gnb.fit(X_train,y_train)
    
    print("\n...",model_gnb.score(X_valid,y_valid))
    print(model_gnb.score(X_train,y_train))
    
    
    #KNN
     
   
    model_knn = make_pipeline( 
        StandardScaler(),
        KNeighborsClassifier(n_neighbors = 10 )
        )
     
    model_knn.fit(X_train,y_train)
    
    print("\n...",model_knn.score(X_valid,y_valid))
    print(model_knn.score(X_train,y_train))
    
    
    #ADA BOOST
  
    model_ab = make_pipeline( 
        StandardScaler(),
        AdaBoostClassifier(n_estimators=80,learning_rate=1, random_state=np.random.seed(1234))
        )
     
    model_ab.fit(X_train,y_train)
    
    print("\n...",model_ab.score(X_valid,y_valid))
    print(model_ab.score(X_train,y_train))
    
    #GRADIENT BOOST

    model_gb = make_pipeline( 
        StandardScaler(),
        GradientBoostingClassifier(n_estimators=100, max_depth=7, min_samples_leaf=0.1, random_state=np.random.seed(1234))
        )     
    model_gb.fit(X_train,y_train)
    
    print("\n...",model_gb.score(X_valid,y_valid))
    print(model_gb.score(X_train,y_train))


if __name__ == '__main__':
    data = pd.read_json('data/osm_food_rating_cleaned.json.gz', orient='records', lines=True)

    X = data[
        ['lat', 'lon', 'num_reviews', 'phone', 'website', 'price', 'chain', 'delivery', 'dine_in', 'drive_through']]
    y = data['rating']

    print(data.columns)

    run_models(X, y)











