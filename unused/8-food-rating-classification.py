# -*- coding: utf-8 -*-
"""
Created on Sun Jul 25 09:37:21 2021

@author: Admin
"""

import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.svm import SVC

#Do not make this FunctionTransformer as it would be applicable to entire dataset
def scale_ratings(data):
    data['rating'] = data['rating'].normalise()
    if data['ratings'] < 0.3:
        data['label']='Avoid'
    if data['rating'] >= 0.3 & data['rating'] < 0.6:
        data['label'] = 'Average'
    if data['rating'] >= 0.6 & data['rating'] <=0.8:
        data['label'] = 'Recommended'
    if data['rating'] > 0.8:
        data['label'] = 'Must Try'
    return data
    
    
    
def main(inputs):
    data = pd.read_json(inputs, orient='records', lines=True)
    print(data)
    mod_data = scale_ratings(data)
    
    X = mod_data[
        ['lat', 'lon', 'num_reviews', 'phone', 'website', 'price', 'chain', 'delivery', 'dine_in', 'drive_through']]
    y = mod_data['label']
    X_train, X_valid, y_train, y_valid = train_test_split(X, y, train_size=0.75 , test_size = 0.25)
    
    # Random Forest Classifier
    RFmodel = RandomForestClassifier(n_estimators=110, criterion='gini',
        max_depth=7, min_samples_leaf=30, bootstrap=True)
    RFmodel.fit(X_train, y_train)
    print(RFmodel.score(X_train, y_train)) #Use Output Template instead
    print(RFmodel.score(X_valid, y_valid))
    
    #SVM Classifier
    SVMmodel = make_pipeline(
            PCA(6),
            SVC(kernel='linear', C=3.0)) #Try scale + PCA
    SVMmodel.fit(X_train, y_train)
    print(SVMmodel.score(X_train,y_train))
    print(SVMmodel.score(X_valid, y_valid))
    
    #MLP Classifier
    MLPmodel = MLPClassifier(solver='lbfgs', hidden_layer_sizes=(3,2),
                             activation='logistic')
    MLPmodel.fit(X_train, y_train)
    print(MLPmodel.coefs_[0])
    print(MLPmodel.intercepts_[0])
    print(MLPmodel.score(X_train, y_train))
    print(MLPmodel.score(X_valid, y_valid))
    
    


if __name__ == '__main__':
    main('data/osm_food_rating_cleaned.json.gz')

