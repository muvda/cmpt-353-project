import pandas as pd
import numpy as np


def fix_datetime(timestamp):
    return pd.to_datetime(timestamp, errors='coerce')


def main():
    data_main = pd.DataFrame()
    for n in range(0, 14):
        inputs = f'data/osm_food_rating{n}.json.gz'
        data = pd.read_json(inputs, orient='records', lines=True)
        data_main = data_main.append(data.copy())
    data_main['timestamp'] = data_main['timestamp'].apply(fix_datetime)
    data_main.to_json('data/osm_food_rating.json.gz', orient='records', lines=True)


if __name__ == '__main__':
    main()
