import pandas as pd
import numpy as np
import geopandas as gpd


def main(inputs):
    geo_data = gpd.read_file(inputs)
    geo_data = geo_data[(geo_data['CENSUS_DIVISION_NAME'] == 'Greater Vancouver')]
    geo_data.to_file('data/VancouverMetro_boundaries.geojson', driver='GeoJSON')


if __name__ == '__main__':
    inputs = 'data/CEN_CENSUS_SUBDIVSIONS_SVW.geojson'
    main(inputs)
