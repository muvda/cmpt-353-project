import pandas as pd
import numpy as np
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.feature_selection import RFECV
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import train_test_split, KFold
from sklearn.preprocessing import StandardScaler
from sklearn.neural_network import MLPRegressor
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV
from sklearn.pipeline import Pipeline
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error
import matplotlib.pyplot as plt
import seaborn as sb

OUTPUT_TEMPLATE = (
    'Gradient Boost Regressor:\n'
    'R2 score: {gb_r2_valid:.3f} (valid), {gb_r2_train:.3f} (train)\n'
    'MAE score: {gb_mae_valid:.3f} (valid), {gb_mae_train:.3f} (train)\n'
    'MSE score: {gb_mse_valid:.3f} (valid), {gb_mse_train:.3f} (train)\n'
    'Random Forest Regressor:\n'
    'R2 score: {rf_r2_valid:.3f} (valid), {rf_r2_train:.3f} (train)\n'
    'MAE score: {rf_mae_valid:.3f} (valid), {rf_mae_train:.3f} (train)\n'
    'MSE score: {rf_mse_valid:.3f} (valid), {rf_mse_train:.3f} (train)\n'
    'MLP Regressor:\n'
    'R2 score: {mlp_r2_valid:.3f} (valid), {mlp_r2_train:.3f} (train)\n'
    'MAE score: {mlp_mae_valid:.3f} (valid), {mlp_mae_train:.3f} (train)\n'
    'MSE score: {mlp_mse_valid:.3f} (valid), {mlp_mse_train:.3f} (train)\n'
)


def random_parameter_tuning(model, params, X, y):
    X_train, X_valid, y_train, y_valid = train_test_split(X, y, random_state=1, test_size=0.3)
    pipe = Pipeline([
        ('scaler', StandardScaler()),
        ('models', model)]
    )
    tuning = RandomizedSearchCV(estimator=pipe, param_distributions=params, n_iter=100, cv=KFold(2), scoring='r2',
                                random_state=np.random.seed(1234), n_jobs=-1)
    tuning.fit(X_train, y_train)
    print(tuning.best_params_)
    print(tuning.best_score_)


def fine_parameter_tuning(model, params, X, y):
    X_train, X_valid, y_train, y_valid = train_test_split(X, y, random_state=1, test_size=0.3)
    pipe = Pipeline([
        ('scaler', StandardScaler()),
        ('models', model)]
    )
    tuning = GridSearchCV(estimator=pipe, param_grid=params, cv=KFold(2), scoring='r2', n_jobs=-1)
    tuning.fit(X_train, y_train)
    print('Best parameters:' + tuning.best_params_)
    print('Score :' + str(tuning.best_score_))


def tuning_models(X, y):
# from https://towardsdatascience.com/hyperparameter-tuning-the-random-forest-in-python-using-scikit-learn-28d2aa77dd74
    print('Begin tuning hyper parameters: ')
    print('Results may be different on each run')
    rf_random_tuning(X, y)
    gb_random_tuning(X, y)
    gb_fine_tuning(X, y)
    mlp_fine_tuning(X, y)


def rf_random_tuning(X, y):
    print('Random Forest Regressor Randomized Tuning:')
    rf_params = {'models__n_estimators': [100, 200, 400, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000],
                 'models__max_depth': [5, 7, 8, 10, 13, 15, None],
                 'models__min_samples_split': [2, 4, 6, 8, 10, 20, 40, 60, 100],
                 'models__min_samples_leaf': [1, 3, 5, 7, 9],
                 'models__max_features': [1, 2, 3, 4, 5]}
    random_parameter_tuning(RandomForestRegressor(random_state=np.random.seed(1234)), rf_params, X, y)


def gb_random_tuning(X, y):
    print('Gradient Boosting Regressor Randomized Tuning:')
    gb_params = {'models__n_estimators': [100, 200, 400, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000],
                 'models__max_depth': [5, 7, 8, 10, 13, 15, None],
                 'models__min_samples_split': [2, 4, 6, 8, 10, 20, 40, 60, 100],
                 'models__min_samples_leaf': [1, 3, 5, 7, 9],
                 'models__max_features': [1, 2, 3, 4, 5],
                 'models__subsample': [0.6, 0.7, 0.8, 0.9],
                 'models__learning_rate': [0.05, 0.1, 0.2]}
    random_parameter_tuning(GradientBoostingRegressor(random_state=np.random.seed(1234)), gb_params, X, y)


def gb_fine_tuning(X, y):
    print('Gradient Boosting Regressor Fine Tuning:')
    gb_params = {'models__n_estimators': [100, 200],
                 'models__max_depth': [3, 5, 7],
                 'models__min_samples_split': [10, 30, 60],
                 'models__min_samples_leaf': [5, 7],
                 'models__max_features': [2, 3, 5],
                 'models__subsample': [0.6, 0.7, 0.8, 0.9],
                 'models__learning_rate': [0.05]}
    fine_parameter_tuning(GradientBoostingRegressor(random_state=np.random.seed(1234)), gb_params, X, y)


def mlp_fine_tuning(X, y):
    print('MLP Regressor Fine Tuning:')
    mlp_params = {'models__hidden_layer_sizes': [(4,), (5,), (6,)],
                  'models__activation': ['relu', 'tanh', 'logistic'],
                  'models__alpha': [0.05, 0.1]}
    fine_parameter_tuning(MLPRegressor(random_state=np.random.seed(1234), solver='lbfgs', max_iter=10000), mlp_params,
                          X, y)


def plot_sort_regressor(name, y_train, y_valid, predict_train, predict_valid):
    valid_df = pd.DataFrame({'y': y_valid, 'predict': predict_valid})
    valid_df = valid_df.sort_values(by='y')
    train_df = pd.DataFrame({'y': y_train, 'predict': predict_train})
    train_df = train_df.sort_values(by='y')

    plt.close("all")
    plt.figure(figsize=(15, 6))
    plt.plot(range(0, y_valid.size), valid_df['y'], 'b.', alpha=0.75)
    plt.plot(range(0, y_valid.size), valid_df['predict'], 'r-')
    plt.title(name + ' Regressor Predictor on the sorted Valid Data')
    plt.ylabel('Rating')
    plt.savefig('figures/Regression/' + name + 'regressor_prediction_valid.png')
    plt.close("all")

    plt.figure(figsize=(15, 6))
    plt.plot(range(0, y_train.size), train_df['y'], 'b.', alpha=0.75)
    plt.plot(range(0, y_train.size), train_df['predict'], 'r-')
    plt.title(name + ' Regressor Predictor on the sorted Train Data')
    plt.ylabel('Rating')
    plt.savefig('figures/Regression/' + name + 'regressor_prediction_train.png')
    plt.close("all")


def validation(gb, rf, mlp, X_train, X_valid, y_train, y_valid):
    print('Begin Validation')
    rf_train_predict = rf.predict(X_train)
    rf_valid_predict = rf.predict(X_valid)
    gb_train_predict = gb.predict(X_train)
    gb_valid_predict = gb.predict(X_valid)
    mlp_train_predict = mlp.predict(X_train)
    mlp_valid_predict = mlp.predict(X_valid)
    print(OUTPUT_TEMPLATE.format(
        gb_r2_valid=r2_score(y_valid, gb_valid_predict),
        gb_r2_train=r2_score(y_train, gb_train_predict),
        gb_mae_valid=mean_absolute_error(y_valid, gb_valid_predict),
        gb_mae_train=mean_absolute_error(y_train, gb_train_predict),
        gb_mse_valid=mean_squared_error(y_valid, gb_valid_predict),
        gb_mse_train=mean_squared_error(y_train, gb_train_predict),
        rf_r2_valid=r2_score(y_valid, rf_valid_predict),
        rf_r2_train=r2_score(y_train, rf_train_predict),
        rf_mae_valid=mean_absolute_error(y_valid, rf_valid_predict),
        rf_mae_train=mean_absolute_error(y_train, rf_train_predict),
        rf_mse_valid=mean_squared_error(y_valid, rf_valid_predict),
        rf_mse_train=mean_squared_error(y_train, rf_train_predict),
        mlp_r2_valid=r2_score(y_valid, mlp_valid_predict),
        mlp_r2_train=r2_score(y_train, mlp_train_predict),
        mlp_mae_valid=mean_absolute_error(y_valid, mlp_valid_predict),
        mlp_mae_train=mean_absolute_error(y_train, mlp_train_predict),
        mlp_mse_valid=mean_squared_error(y_valid, mlp_valid_predict),
        mlp_mse_train=mean_squared_error(y_train, mlp_train_predict),
    ))

    plot_sort_regressor('GradientBoosting', y_train, y_valid, gb_train_predict, gb_valid_predict)
    plot_sort_regressor('RandomForest', y_train, y_valid, rf_train_predict, rf_valid_predict)
    plot_sort_regressor('MLP', y_train, y_valid, mlp_train_predict, mlp_valid_predict)


def run_different_model(X, y):
    print('Begin model building')
    X_train, X_valid, y_train, y_valid = train_test_split(X, y, random_state=1, test_size=0.3)
    print('Building Gradient Boosting Regressor model')
    gb_model = make_pipeline(
        StandardScaler(),
        GradientBoostingRegressor(n_estimators=100, min_samples_split=30, min_samples_leaf=5, max_features=3,
                                  max_depth=5, learning_rate=0.05, subsample=0.9, random_state=np.random.seed(1234))
    )
    print('Building Random Forest Regressor model')
    rf_model = make_pipeline(
        StandardScaler(),
        RandomForestRegressor
        (n_estimators=2000, min_samples_split=8, min_samples_leaf=1, max_features=4, max_depth=10, bootstrap=True,
         random_state=np.random.seed(1234), n_jobs=-1)
    )
    print('Building MLP Regressor model')
    mlp_model = make_pipeline(
        StandardScaler(),
        MLPRegressor(hidden_layer_sizes=(5,),
                     activation='relu', solver='lbfgs', random_state=np.random.seed(1234), max_iter=1000, alpha=0.05)
    )
    gb_model.fit(X_train, y_train)
    rf_model.fit(X_train, y_train)
    mlp_model.fit(X_train, y_train)
    validation(gb_model, rf_model, mlp_model, X_train, X_valid, y_train, y_valid)


def plot_feature_important(estimator, features, X_train, y_train):
# From https://scikit-learn.org/stable/auto_examples/ensemble/plot_forest_importances.html
    estimator.fit(X_train, y_train)
    importances = estimator.feature_importances_
    std = np.std([
        tree.feature_importances_ for tree in estimator.estimators_], axis=0)
    sb.set()
    plt.close("all")
    plt.figure(figsize=(8, 6))
    plt.xticks(rotation=25)
    plt.bar(features, importances, yerr=std)
    plt.ylabel("Mean decrease in impurity")
    plt.title('Feature importances using MDI')
    plt.savefig('figures/Regression/regressor_feature_importances.png')
    plt.close("all")


def features_selection_rfecv(X, y):
    print('Begin feature selection')
    features = X.columns.tolist()
    X_norm = StandardScaler().fit_transform(X)
    X_train, X_valid, y_train, y_valid = train_test_split(X_norm, y, random_state=1, test_size=0.3)
    rfecv_estimator = RandomForestRegressor(random_state=1)
    plot_feature_important(rfecv_estimator, features, X_train, y_train)
    rfecv_selector = RFECV(estimator=rfecv_estimator, cv=KFold(2), scoring='r2', min_features_to_select=1)
    rfecv_selector = rfecv_selector.fit(X_train, y_train)
    mask = rfecv_selector.get_support()
    selected_features = X.loc[:, mask].columns.tolist()
    print('Selected features are ' + ', '.join(selected_features))
    return selected_features


def main(inputs):
    sb.set()
    data = pd.read_json(inputs, orient='records', lines=True)
    X = data[
        ['lat', 'lon', 'num_reviews', 'phone', 'website', 'price', 'chain', 'delivery', 'dine_in', 'drive_through']]
    y = data['rating']
    chosen_features = features_selection_rfecv(X, y)
    X_select = X[chosen_features].to_numpy()
    # Tuning will take a long time if uncomment
    # tuning_models(X_select, y.to_numpy())
    run_different_model(X_select, y.to_numpy())


if __name__ == '__main__':
    main('data/osm_food_rating_cleaned.json.gz')
