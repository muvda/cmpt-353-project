# CMPT 353 Project
## Guide to the structure
- <b>data:</b> contains the data the project used and created. The data inside will be covered later.
- <b>figures:</b> contains figures created during the project. The sub-folders and figures inside will be cover later.
- <b>photo:</b> contains the photos used by <i>2-photo-amenity.py</i>
- <b>unused:</b> contains obsolete data and python files that wasn't used in the project. 
- <b>reproducible_results:</b> contains terminal result for running the Python files, should be reproducible.
## Required libraries
The libraries required is already included in <i>requirements.txt</i>. Still, here are the list of libraries we used:
- pandas
- numpy
- geopandas
- exifread
- piexif
- scikit-learn
- gpsphoto
- matplotlib
- seaborn
- scipy
- statsmodels
- Shapely
## Order of execution
Beside <i>3-obtain-rating-data.py</i>, which required a paid API to run, the order is as follows :
1. 1-osm-data-cleaning.py
1. 2-photo-amenity.py (optional)
1. 4-data-rating-extract.py
1. 5-rating-cleaning.py
1. 6-rating-regression.py
1. 7-rating-classifcation.py
1. 8-price-semi-classification.py
1. 9-stats-questions.py
## Guide to Python files
### 1-osm-data-cleaning.py
- Description: Clean the initial OSM data 
- Expected inputs: `python3 1-osm-data-cleaning.py`
- Used data: `data/amenities-vancouver.json.gz`
- Data outputs: `data/osm_tourist.json.gz`; `data/osm_food.json.gz`

### 2-photo-amenity.py
- Description: Find out amenities nearby a photo location
- Expected inputs: `python3 2-photo-amenity.py <photo location> <distance (in meter)>`
- Used data: `data/osm_tourist.json.gz`
- Example inputs: ```python3 2-photo-amenity.py photo/photo4.jpg 400```
- Example outputs:
![result-2](./reproducible_results/Result-2.png)

### 3-obtain-rating-data.py (DO NOT RUN)
- Description: Obtain raw data for restaurants
- Used data: `data/osm_food.json.gz`
- Data outputs: `data/osm_food_rating.json.gz`

### 4-data-rating-extract.py
- Description: Extract rating data (and relevant features) for restaurants 
- Expected inputs: `python3 4-data-rating-extract.py`
- Used data: `data/osm_food_rating.json.gz`
- Data outputs: `data/osm_food_rating_extract.json.gz`
- Expected outputs (may take up to 20 minutes):
![result-4](./reproducible_results/Result-4.png)

### 5-rating-cleaning.py
- Description: Clean rating data, create features relevant for other Python files
- Expected inputs: `python3 5-rating-cleaning.py`
- Used data: `data/osm_food_rating_extract.json.gz`
- Data outputs: `data/osm_food_rating_cleaned.json.gz`
- Figures outputs: inside `/figures/Cleaning/`
- Expected outputs:
![result-5](./reproducible_results/Result-5.png)

### 6-rating-regression.py
- Description: Build regression models to predict rating value
- Expected inputs: `python3 6-rating-regression.py`
- Used data: `data/osm_food_rating_extract.json.gz`
- Figures outputs: inside `/figures/Regression/`
- Expected outputs:
![result-6](./reproducible_results/Result-6.png)
<b>NOTE:</b> Hyperparameters tuning is commented out due to long run time. To enable, uncomment ```tuning_models(X_select, y.to_numpy())``` at line 237

### 7-rating-classifcation.py
- Description: Build classification models to predict rating label
- Expected inputs: `python3 7-rating-classifcation.py`
- Used data: `data/osm_food_rating_extract.json.gz`
- Figures outputs: inside `/figures/Classification/`
- Expected outputs:
![result-7](./reproducible_results/Result-7.png)

### 8-price-semi-classification.py
- Description: Build Semi-supervised algorithm to impute price feature and compare to Median imputation
- Expected inputs: `python3 8-price-semi-classification.py`
- Used data: `data/osm_food_rating_extract.json.gz`
- Figures outputs: inside `/figures/Semi_classify/`
- Expected outputs:
![result-8](./reproducible_results/Result-8.png)

### 9-stats-questions.py
- Description: Ask relevant statistic questions
- Expected inputs: `python3 9-stats-questions.py`
- Used data: `data/osm_food_rating_extract.json.gz`
- Figures outputs: inside `/figures/Stats/`
- Expected outputs:
![result-9](./reproducible_results/Result-9.png)
