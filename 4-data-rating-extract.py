import pandas as pd
import numpy as np
import geopandas as gpd
from shapely.geometry import Point


def extract_result(record):
    result = record['result']
    if 'rating' in result:
        record['rating'] = result['rating']
    else:
        record['rating'] = None
    if 'reviews' in result:
        record['num_reviews'] = result['reviews']
    else:
        record['num_reviews'] = None
    if 'address' in result:
        record['address'] = result['address']
    else:
        record['address'] = None
    if 'phone' in result:
        record['phone'] = result['phone']
    else:
        record['phone'] = None
    if 'service_options' in result:
        record['service_options'] = result['service_options']
    else:
        record['service_options'] = None
    if 'website' in result:
        record['website'] = result['website']
    else:
        record['website'] = None
    if 'price' in result:
        record['price'] = result['price']
    else:
        record['price'] = None
    return record


def find_chain(data):
    chain_name = []
    chain_wikidata = []
    chain = []
    for index, row in data.iterrows():
        tags = row['tags']
        if 'brand:wikidata' in tags and row['name'] not in chain_name:
            chain_name.append(row['name'])
            chain.append(1)
            chain_wikidata.append(tags['brand:wikidata'])
    chain_restaurant = pd.DataFrame({'name': chain_name, 'wikidata': chain_wikidata, 'chain': chain})
    return chain_restaurant


def extract_services(record):
    services = record['service_options']
    record['delivery'] = False
    record['dine_in'] = True
    record['drive_through'] = False
    if services is not None:
        if "delivery" in services:
            record['delivery'] = services['delivery']
        elif "no_contact_delivery" in services:
            record['delivery'] = services['no_contact_delivery']
        if "dine_in" in services:
            record['dine_in'] = services['dine_in']
        if "drive_through" in services:
            record['drive_through'] = services['drive_through']
    return record


def find_city(data, geo_data):
    def get_city(record, geo_data):
        point = Point(record['lon'], record['lat'])
        polygon = geo_data['geometry']
        result = polygon.contains(point)
        filtered_gd = geo_data[result.values]
        if len(filtered_gd) == 1:
            div = filtered_gd.iloc[0]['CENSUS_SUBDIVISION_NAME']
            return div
        else:
            return 'Other'

    working_data = data.copy()
    working_data['subdivision'] = working_data.apply(get_city, geo_data=geo_data, axis=1).copy()
    return working_data


def main(input1, input2):
    data = pd.read_json(input1, orient='records', lines=True)
    geo_data = gpd.read_file(input2)
    cleaned_data = data.dropna().copy()
    print('Extract result column')
    cleaned_data = cleaned_data.apply(extract_result, axis=1)
    print('Create chain column')
    chain_restaurant = find_chain(cleaned_data)
    cleaned_data = cleaned_data.merge(chain_restaurant, how='left', on='name')
    cleaned_data = cleaned_data.apply(extract_services, axis=1)
    print('Create subdivision column')
    cleaned_data = find_city(cleaned_data, geo_data)
    cleaned_data = cleaned_data.drop(columns=['result', 'tags', 'service_options'])
    cleaned_data.to_json('data/osm_food_rating_extract.json.gz', orient='records', lines=True)


if __name__ == '__main__':
    main('data/osm_food_rating.json.gz', 'data/CEN_CENSUS_SUBDIVSIONS_SVW.geojson')
