import pandas as pd
import numpy as np
from scipy import stats
from statsmodels.stats.multicomp import pairwise_tukeyhsd
import matplotlib.pyplot as plt
import seaborn as sb

CHAIN_TEMPLATE = (
    "T-test p-value: {initial_ttest_p:.3g}\n"
    "Original data normality p-values: {initial_chain_normality_p:.3g} {initial_non_chain_normality_p:.3g}\n"
    "Original data equal-variance p-value: {initial_levene_p:.3g}\n"
    "Transformed data normality p-values: {transformed_chain_normality_p:.3g} {transformed_non_chain_normality_p:.3g}\n"
    "Transformed data equal-variance p-value: {transformed_levene_p:.3g}\n"
    "Subdivision data normality p-values: {div_chain_normality_p:.3g} {div_non_chain_normality_p:.3g}\n"
    "Subdivision data equal-variance p-value: {div_levene_p:.3g}\n"
    "Subdivision T-test p-value: {div_ttest_p:.3g}\n"
    "Subdivision one-tailed T-test p-value: {div_ttest_one_tailed_p:.3g}"
)

REGION_TEMPLATE = (
        "Vancouver mean: {vancouver_mean:.3g} normality p-values: {vancouver_norm_p:.3g}\n" +
        "Burnaby mean: {burnaby_mean:.3g} normality p-values: {burnaby_norm_p:.3g}\n" +
        "Richmond mean: {richmond_mean:.3g} normality p-values: {richmond_norm_p:.3g}\n" +
        "Surrey mean: {surrey_mean:.3g} normality p-values: {surrey_norm_p:.3g}\n" +
        "Abbotsford mean: {abbotsford_mean:.3g} normality p-values: {abbotsford_norm_p:.3g}\n" +
        "Langley mean: {langley_mean:.3g} normality p-values: {langley_norm_p:.3g}\n" +
        "ANOVA test p-value: {anova_p:.3g}"
)

NUM_REVIEWS_TEMPLATE = (
        "Correlation coefficient: {corr:.3g}\n" +
        "Fit line slope: {slope:.3g} intercept: {intercept:.3g}\n" +
        "Residuals normality p-values: {residuals_norm_p:.3g}\n" +
        "OLS test p-value: {ols_p:.3g}"
)


def plot_region(vancouver, burnaby, richmond, surrey, abbotsford, langley):
    plt.figure(figsize=(10, 8))
    plt.subplot(2, 3, 1)
    plt.hist(vancouver)
    plt.title("Vancouver Restaurant Rating")
    plt.ylabel('Frequency')
    plt.subplot(2, 3, 2)
    plt.hist(burnaby)
    plt.title("Burnaby Restaurant Rating")
    plt.subplot(2, 3, 3)
    plt.hist(richmond)
    plt.title("Richmond Restaurant Rating")
    plt.subplot(2, 3, 4)
    plt.hist(surrey)
    plt.title("Surrey Restaurant Rating")
    plt.xlabel('Rating')
    plt.ylabel('Frequency')
    plt.subplot(2, 3, 5)
    plt.hist(abbotsford)
    plt.title("Abbotsford Restaurant Rating")
    plt.xlabel('Rating')
    plt.subplot(2, 3, 6)
    plt.hist(langley)
    plt.title("Langley Restaurant Rating")
    plt.xlabel('Rating')
    plt.savefig('./figures/Stats/distribution_of_rating_in_major_cities.png')


def plot_region_count(region):
    region = region[region > 200]
    plt.close('all')
    plt.bar(region.index, region)
    plt.title("No. of restaurants in subdivisions")
    plt.ylabel('No. of restaurants')
    plt.savefig('./figures/Stats/distribution_restaurants_in_major_cities.png')


def region_question(data):
    plot_region_count(data['subdivision'].value_counts())
    print('Is there any difference in restaurant ratings in major regions within Greater Vancouver?')
    vancouver = data[data['subdivision'] == 'Vancouver']
    burnaby = data[data['subdivision'] == 'Burnaby']
    richmond = data[data['subdivision'] == 'Richmond']
    surrey = data[data['subdivision'] == 'Surrey']
    abbotsford = data[data['subdivision'] == 'Abbotsford']
    langley = data[data['subdivision'] == 'Langley']
    plot_region(vancouver['rating'], burnaby['rating'], richmond['rating'], surrey['rating'], abbotsford['rating'],
                langley['rating'])
    print(REGION_TEMPLATE.format(
        vancouver_mean=np.mean(vancouver['rating']),
        vancouver_norm_p=stats.normaltest(vancouver['rating']).pvalue,
        burnaby_mean=np.mean(burnaby['rating']),
        burnaby_norm_p=stats.normaltest(burnaby['rating']).pvalue,
        richmond_mean=np.mean(richmond['rating']),
        richmond_norm_p=stats.normaltest(richmond['rating']).pvalue,
        surrey_mean=np.mean(surrey['rating']),
        surrey_norm_p=stats.normaltest(surrey['rating']).pvalue,
        abbotsford_mean=np.mean(abbotsford['rating']),
        abbotsford_norm_p=stats.normaltest(abbotsford['rating']).pvalue,
        langley_mean=np.mean(langley['rating']),
        langley_norm_p=stats.normaltest(langley['rating']).pvalue,
        anova_p=stats.f_oneway(vancouver['rating'], burnaby['rating'], richmond['rating'], surrey['rating'],
                               abbotsford['rating'], langley['rating']).pvalue
    ))
    rating_data = pd.DataFrame(
        {'Vancouver': vancouver['rating'], 'Burnaby': burnaby['rating'], 'Richmond': richmond['rating'],
         'Surrey': surrey['rating'], 'Abbotsford': abbotsford['rating'], 'Langley': langley['rating']})
    rating_melt = pd.melt(rating_data)
    rating_melt = rating_melt.dropna()
    posthoc = pairwise_tukeyhsd(
        rating_melt['value'], rating_melt['variable'],
        alpha=0.05)
    print(posthoc)
    plt.close('all')
    fig = posthoc.plot_simultaneous()
    fig.savefig('./figures/Stats/post_hoc_analysis_region_rating.png')


def plot_chain(chain, non_chain):
    plt.close('all')
    plt.hist([non_chain, chain])
    plt.title('Distribution of rating between chain and non-chain restaurant')
    plt.legend(['Non-chain', 'Chain'])
    plt.xlabel('Rating')
    plt.ylabel('Frequency')
    plt.savefig('./figures/Stats/distribution_of_rating_in_chain_and_non_chain.png')
    plt.close('all')


def chain_question(data):
    print('Is there any difference in rating between chain and non-chain restaurants in each region?')
    chain = data[data['chain'] == 1]
    non_chain = data[data['chain'] == 0]
    plot_chain(chain['rating'], non_chain['rating'])
    transformed_chain = chain['rating'] ** 2
    transformed_non_chain = non_chain['rating'] ** 2
    division_chain = chain.groupby(['subdivision']).aggregate({'rating': 'mean'}).reset_index()
    division_non_chain = non_chain.groupby(['subdivision']).aggregate({'rating': 'mean'}).reset_index()
    print(CHAIN_TEMPLATE.format(
        initial_ttest_p=stats.ttest_ind(chain['rating'], non_chain['rating']).pvalue,
        initial_chain_normality_p=stats.normaltest(chain['rating']).pvalue,
        initial_non_chain_normality_p=stats.normaltest(non_chain['rating']).pvalue,
        initial_levene_p=stats.levene(chain['rating'], non_chain['rating']).pvalue,
        transformed_chain_normality_p=stats.normaltest(transformed_chain).pvalue,
        transformed_non_chain_normality_p=stats.normaltest(transformed_non_chain).pvalue,
        transformed_levene_p=stats.levene(transformed_chain, transformed_non_chain).pvalue,
        div_chain_normality_p=stats.normaltest(division_chain['rating']).pvalue,
        div_non_chain_normality_p=stats.normaltest(division_non_chain['rating']).pvalue,
        div_levene_p=stats.levene(division_chain['rating'], division_non_chain['rating']).pvalue,
        div_ttest_p=stats.ttest_ind(division_chain['rating'], division_non_chain['rating']).pvalue,
        div_ttest_one_tailed_p=stats.ttest_ind(division_chain['rating'], division_non_chain['rating'], alternative='less').pvalue
    ))


def plot_num_reviews(x, y, y_pred):
    plt.plot(x, y, 'b.', alpha=0.5)
    plt.plot(x, y_pred, 'r-', linewidth=3)
    plt.legend(['Observed rating', 'Predict rating'], loc='lower right')
    plt.title('Observed rating and rating predicted by Linear Regression')
    plt.ylabel('Rating Value')
    plt.xlabel('No. of reviews')
    plt.savefig('./figures/Stats/num_reviews_ratings.png')


def plot_residuals(y, y_pred):
    plt.close('all')
    plt.figure()
    residuals = y - y_pred
    plt.hist(residuals)
    plt.ylabel('Frequency')
    plt.xlabel('Residuals Value')
    plt.title('Rating Residuals histogram')
    plt.savefig('./figures/Stats/rating_residuals.png')


def num_reviews_question(data):
    print("Is the rating value correlated with the number of reviews given?")
    plt.close('all')
    plt.figure()
    x = data['num_reviews']
    y = data['rating']
    reg = stats.linregress(x, y)
    y_pred = reg.slope * x + reg.intercept
    plot_num_reviews(x, y, y_pred)
    plot_residuals(y, y_pred)
    print(NUM_REVIEWS_TEMPLATE.format(
        corr=reg.rvalue,
        slope=reg.slope,
        intercept=reg.intercept,
        residuals_norm_p=stats.normaltest(y - y_pred).pvalue,
        ols_p=reg.pvalue
    ))


def main(input1):
    sb.set()
    data = pd.read_json(input1, orient='records', lines=True)
    num_reviews_question(data)
    region_question(data)
    chain_question(data)


if __name__ == '__main__':
    main('data/osm_food_rating_cleaned.json.gz')
