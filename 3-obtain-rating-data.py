import pandas as pd
import numpy as np
from serpapi import GoogleSearch

API = "Secret_API_Code"


def query_result(record):
    name = record['name']
    lon = record['lon']
    lat = record['lat']
    ll = "@" + str(lat) + "," + str(lon) + ",20z"
    params = {
        "engine": "google_maps",
        "q": name,
        "type": "search",
        "ll": ll,
        "api_key": API
    }

    client = GoogleSearch(params)
    data = client.get_dict()
    if 'local_results' in data:
        for result in data['local_results']:
            return result
    elif 'place_results' in data:
        return data['place_results']
    return None


def main(inputs):
    data = pd.read_json(inputs, orient='records', lines=True)
    data_website = data.copy()
    data_website['result'] = data_website.apply(query_result, axis=1)
    data_website.to_json('data/osm_food_rating.json.gz', orient='records', lines=True)


if __name__ == '__main__':
    inputs = 'data/osm_food.json.gz'

    main(inputs)
